'use strict';

const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const db = [
    {
        id: 1,
        name: 'Games Importadora SA',
        description: 'Importadora de games e acessórios.',
        cnpj: '48.161.486/0001-76',
        createdAt: "2019-08-18T21:37:54.815Z"
    },
    {
        id: 2,
        name: 'Consoles e Jogos LTDA',
        description: 'Fornecedor de aparelhos videogames e jogos.',
        cnpj: '65.783.342/0001-70',
        createdAt: "2019-08-18T21:37:54.815Z"
    }
];
var idItem = 2;

const itemSchema = Joi.object({
    id: Joi.number().required(),
    name: Joi.string().required(),
    description: Joi.string(),
    cnpj: Joi.string().required(),
    createdAt: Joi.date().iso(),
    modifiedAt: Joi.date().iso()
});


const init = async () => {

    const server = Hapi.server({
        port: 3002,
        host: 'localhost'
    });

    server.route({
        method: 'POST',
        path: '/provider',
        handler: (request, h) => {
            idItem = idItem + 1;
            const item = Object.assign({
                ...request.payload
            }, {
                createdAt: new Date(),
                id: idItem
            })

            db.push(item);
            return item;
        },
        options: {
            validate: {
                payload: {
                    name: Joi.string().required(),
                    description: Joi.string().min(0),
                    cnpj: Joi.string().required()
                }
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/provider',
        handler: (request, h) => {
            return db;
        },
        options: {
            response: {
                schema: Joi.array().items(itemSchema),
                failAction: 'log'
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/provider/{id}',
        handler: (request, h) => {
            const result = db.filter(item => item.id == request.params.id)[0];
            if(!result) {
                return h.status(404);
            }
            return result;
        }        
    })

    server.route({
        method: 'PUT',
        path: '/provider/{id}',
        handler: (request, h) => {
            console.log('calling put with payload ', request.payload)
            console.log('params ', request.params)
            let payload = request.payload
            let item = db
                .filter(f => f.id == request.params.id)
                .map(m => {
                    m.name = payload.name;
                    m.description = payload.description;
                    m.cnpj = payload.cnpj;
                    m.modifiedAt = new Date();
                    return m;
                })[0];
            return item;
        },
        options: {
            validate: {
                payload: {
                    id: Joi.number().required(),
                    name: Joi.string().required(),
                    description: Joi.string().min(0),
                    cnpj: Joi.string().required(),
                    createdAt: Joi.date().iso().required(),
                    modifiedAt: Joi.date().iso()
                }
            }
        }
    })

    server.route({
        method: 'DELETE',
        path: '/provider',
        handler: (request, h) => {
            let item;
            let i;
            for (i = 0; i < db.length; i++) {
                if (db[i].id == request.payload.id) {
                    item = Object.assign(db[i], item);
                    db.splice(i, 1);
                }
            }

            if (!item) {
                return Boom.notFound('Item not found');
            }

            return item;
        },
        options: {
            validate: {
                payload: {
                    id: Joi.number().required()
                }
            }
        }
    })

    await server.start();
    console.log('Provider running on %s', server.info.uri);
};


init();